## 0. pacemaker 용어
- pacemaker: 클러스터 노드 모니터링, 서비스와 리소스 관리
- corosync: 클러스터 노드간의 통신 담당
- pcs: pcs 명령어 도구(클러스터 및 리소스 생성 도구)와 pcsd 서비스(pacemaker와 corosync 구성 동기화)
- fence: 데이터 무결성을 유지하기 위해 사용. 기본 동작은 재시작

## 1. 패키지 설치 및 서비스 실행 - 모든 노드
### 0) /etc/hosts 파일
```
vi /etc/hosts
...
10.0.0.11   node1
10.0.0.12   node2
10.0.0.13   node3
```

### 1) 패키지 설치 
```bash
yum -y insatll pacemaker pcs
```

### 2) 서비스 실행 
```bash
systemctl start pcsd
systemctl enable pcsd
```

### 3) 서비스 오픈
```bash
firewall-cmd --add-service=high-availability 
firewall-cmd --add-service=high-availability --permanent
```

### 4) hacluster 사용자 암호 지정
```bash
echo 'dkagh1.' | passwd --stdin hacluster
```




## 2. 클러스터 생성 및 실행 - node1
### 1) 노드 인증
> pcs cluster auth [OPTION] NODE...
```bash
pcs cluster auth node1 node2 node3
Username: hacluster
Password: 패스워드
```

### 2) 클러스터 생성
> pcs cluster setup [OPTION] NODE...
```bash
pcs cluster setup --name cluster1 --enable \
node1 node2 node3
```

### 3) 클러스터 실행
> pcs cluster start [OPTION]
```bash
pcs cluster start --all
```

### 4) 클러스터 상태확인
> pcs cluster status
```bash
pcs status cluster
```

> pcs cluster corosync
```bash
pcs cluster corosync
```

## 3. apache 리소스 생성
### 0) stonith 비활성화
```bash
pcs property set stonith-enabled=false
```

### 1) httpd 패키지 설치 - 모든 노드
```bash
yum -y install httpd
firewall-cmd --add-service=http
firewall-cmd --add-service=http --permanent
```

### 2) 리소스 생성
```bash
pcs resource create webip IPaddr2 ip=10.0.0.10 cidr_netmask=24 --group=webrsc
pcs resource create websvc apache --group=webrsc
```
